var recipes = [{
  "types": ["life", "hammer"],
  "result": "grain"
},
{
  "types": ["water", "dirt"],
  "result": "mud"
},
{
  "types": ["life", "grain"],
  "result": "livestock"
},
{
  "types": ["livestock", "mud"],
  "result": "pig"
},
{
  "types": ["life", "water"],
  "result": "tomato"
},
{
  "types": ["hammer", "heat"],
  "result": "metal"
},
{
  "types": ["milk", "heat"],
  "result":  "cheese"
},
{
  "types": ["hammer", "livestock"],
  "result": "meat"
},
{
  "types": ["hammer", "metal"],
  "result": "bucket"
},
{
  "types": ["bucket", "heat"],
  "result": "metal"
},
{
  "types": ["dough", "heat"],
  "result": "bread"
},
{
  "types": ["hammer", "grain"],
  "result": "flour"
},
{
  "types": ["flour", "water"],
  "result": "dough"
},
{
  "types": ["livestock", "bucket"],
  "result": "milk"
},
{
  "types": ["dough", "hammer"],
  "result": "pizza dough"
},
{
  "types": ["tomato", "hammer"],
  "result": "tomato sauce"
},
{
  "types": ["pizza dough", "tomato sauce"],
  "result": "red sauce dough"
},
{
  "types": ["red sauce dough", "cheese"],
  "result": "cheesy dough"
},
{
  "types": ["cheesy dough", "meat"],
  "result": "uncooked pizza"
},
{
  "types": ["uncooked pizza", "heat"],
  "result": "pizza"
},
{
  "types": ["pig", "hammer"],
  "result": "ham"
},
{
  "types": ["mud", "life"],
  "result": "pineapple"
},
{
  "types": ["pineapple", "pizza", "ham"],
  "result": "hawaiian pizza"
},
{
  "types": ["dough", "dough"],
  "result": "deep dish crust"
}];

define([], function() {
    return recipes;
});