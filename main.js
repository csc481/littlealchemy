// This grabs all the code that helps the game work
requirejs(['../engine/Mochi', './element'], function (Mochi, core) {
    /**
     * The main class that runs the little alchemy pizzeria game.
     */
    class Pizzeria {
        constructor() {
            this.game = new Mochi.Game({
                width: window.innerWidth,
                height: window.innerHeight,
                fps: 60,
                startState: "main"
            });
            // Attach our handlers.

            // We have two data files that hold info about the game
            this.recipeData = new Mochi.Content.Asset('data/recipes.js');
            this.typeData = new Mochi.Content.Asset('data/types.js');
            // The list of all elements we'll work with
            this.types = new core.ElementList(0, this.game.height);
            // A list of all elements that currently exist.
            this.elements = [];
            // The currently selected object.
            this.selected = null;
            this.game.addState(
                new Mochi.GameState("main").setup(this.update.bind(this), this.draw.bind(this))
            );

        }


        /**
         * Loads the game's assets before starting, then starts
         * the game.
         */
        init() {
            // Load all our data
            this.game.log("Loading game data...");
            Mochi.Content.LoadAllAssets([this.typeData, this.recipeData]).then(function(result) {
                this.game.log(result);
                let assets = this.makeTypes(this.typeData.data);
                this.addRecipes(this.recipeData.data);
                // Initialize bindings to game objects.
                this.initBindings();
                // Make sure all our assets are loaded
                Mochi.Content.LoadAllAssets(assets).then(function(result) {
                    // Start the game
                    this.game.log("All assets loaded.");
                    this.game.log(result);
                    this.game.start();
                }.bind(this));
            }.bind(this));
        };

        /**
         * Renders the pizzeria game
         * @param ctx The context needed.
         */
        draw(ctx) {
            ctx.save();
            ctx.clearRect(0, 0, this.game.width, this.game.height);
            // Draws our available element list.
            this.types.draw(ctx);
            for (let i = 0; i < this.elements.length; i++) {
                this.elements[i].draw(ctx);
            }
            ctx.restore();
        }

        /** The update loop */
        update(dt) {

        }

        /** Creates types from stored data
         * @return A list of assets that need loading. */
        makeTypes(data) {
            this.game.log("Gathering types from data...");
            let assets = [];
            for (let i = 0; i < data.length; i++) {
                let sprite = new Mochi.Graphics.Sprite(data[i].image, { height: 74, width: 74 });
                this.types.add(new core.ElementType( {name: data[i].name, sprite: sprite, available: data[i].available}));
                assets.push(sprite);
            }
            return assets;
        }

        /** Creates recipes from stored data */
        addRecipes(data) {
            this.game.log("Gathering recipes from data...");
            for (let i = 0; i < data.length; i++) {
                if (data[i].types.length === 2)
                    this.types.addReversibleRecipeByName(data[i].types[0], data[i].types[1], null, data[i].result);
                else if (data[i].types.length === 3)
                    this.types.addReversibleRecipeByName(data[i].types[0], data[i].types[1], data[i].types[2], data[i].result);
            }
        }

        /**
         * Initializes all the input bindings to attach to game functions.
         */
        initBindings() {
            this.game.input.onMouseDown(this.checkForType.bind(this));
            this.game.input.onMouseDown(this.checkForSelect.bind(this));
            this.game.input.onMove(this.moveSelected.bind(this));
            this.game.input.onMouseUp(this.checkForCombinations.bind(this));
        }

        checkForType(evt, x, y) {
            // Check to see if the person clicked a type
            let type = this.types.getTypeAtCoords(x, y);
            if (type == null)
                return;
            // we want to generate a new element at that position for them
            let element = new core.Element(type, x,y);
            element.init();
            this.elements.push(element);
            this.selected = element;
        }

        checkForSelect(evt, x, y) {
            // Loop through backwards to ensure top one is chosen.
            for (let i = this.elements.length - 1; i >= 0; i--) {
                if (this.elements[i].containsPoint(x, y)) {
                    this.selected = this.elements[i];
                    return;
                }
            }
        }

        moveSelected(evt, dx, dy) {
            if (!this.game.input.mouseDown || this.selected === null)
                return;
            // When the mouse is moving and the mouse is down, check for elements under
            this.selected.setCenter(dx, dy);
        }

        checkForCombinations(evt, x, y) {
            let current = this.selected;
            this.selected = null;
            // When we release, if there is a type under us, we should be returned (deleted)
            if (current === null)
                return;
            let typeUnder = this.types.getTypeUnderElement(current);
            if (typeUnder !== null) {
                let cIndex = this.elements.indexOf(current);
                this.elements.splice(cIndex, 1);
                return;
            }
            // otherwise when we release the mouse, if there are two or three elements colliding, try to combine.
            let under1 = null;
            let under2 = null;
            let underIndex = 0;
            let underIndex2 = 0;
            console.log("Checking");
            for (let i = this.elements.length - 1; i >= 0; i--) {
                if (this.elements[i] !== current && this.elements[i].containsPoint(x, y)) {
                    if(under1 === null) {
                        under1 = this.elements[i];
                        underIndex = i;
                    }
                    else if(under2 === null) {
                        under2 = this.elements[i];
                        underIndex2 = i;
                        break;
                    }
                }
            }
            console.log("Checking2");
            // Return if we have nothing under us, we'll just drop it.
            if (under1 === null)
                return;
            console.log("Checking3");
            // check if under & current have a recipe for a combo
            let result = this.types.getRecipeFor(current, under1, under2);
            console.log(result);
            if (result === null)
                return;
            // we have a recipe, time to produce it using the current.
            result.discover();
            current.type = result;
            // now remove under
            this.elements.splice(underIndex, 1);
            if(under2 !== null) {
                this.elements.splice(underIndex2, 1);
            }
        }
    }

    // Create a new pizzeria
    let pizzeria = new Pizzeria();
    // Initialize and begin the game.
    pizzeria.init();

});
